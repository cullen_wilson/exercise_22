﻿using System;

namespace exercise_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5] {"Terminator", "Karate Kid", "Avatar", "Forest Gump", "Dumb and Dumber"};
            Console.WriteLine(string.Join (",",movies));
        }
    }
}
